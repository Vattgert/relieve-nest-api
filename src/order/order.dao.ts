import { Inject } from '@nestjs/common';
import { DATABASE_SERVICE } from 'src/storage/database/database.constants';
import { DatabaseService } from 'src/storage/database/database.service';
import {
  INSERT_ORDER_INFO,
  generateInsertOrderVariationsQuery,
} from './order.queries';

export class OrderDAO {
  constructor(
    @Inject(DATABASE_SERVICE)
    private databaseService: DatabaseService,
  ) {}

  private async insertOrderInfo(activityOrderInformation) {
    const { activityId, scheduleId, userId, hostId } = activityOrderInformation;

    const { id } = await this.databaseService.queryOne({
      query: INSERT_ORDER_INFO,
      params: [activityId, scheduleId, userId, hostId],
    });

    return id;
  }

  private async insertOrderVariations(orderId, variations) {
    const mappedVariations = this.mapVariationsToInsertableView(
      orderId,
      variations,
    );

    const insertOrderVariationsQuery = generateInsertOrderVariationsQuery(
      mappedVariations,
    );

    await this.databaseService.queryAll({
      query: insertOrderVariationsQuery,
    });
  }

  async insertOrder(activityOrderInformation): Promise<number> {
    const createdOrderId = await this.databaseService.transaction(async () => {
      const { variations } = activityOrderInformation;
      const createdOrderId = await this.insertOrderInfo(
        activityOrderInformation,
      );
      await this.insertOrderVariations(createdOrderId, variations);

      return createdOrderId;
    });

    return parseInt(createdOrderId) || 0;
  }

  private mapVariationsToInsertableView(orderId, variations) {
    const variationsValues = [];
    for (const variation of variations) {
      const variationValue = [];
      variationValue.push(orderId);
      variationValue.push(variation.id);
      variationValue.push(variation.quantity);
      variationsValues.push(variationValue);
    }
    return variationsValues;
  }
}
