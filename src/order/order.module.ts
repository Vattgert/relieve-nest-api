import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/storage';
import { OrderDAO } from './order.dao';
import { OrderService } from './order.service';

@Module({
  imports: [DatabaseModule],
  providers: [OrderService, OrderDAO],
  exports: [OrderService],
})
export class OrderModule {}
