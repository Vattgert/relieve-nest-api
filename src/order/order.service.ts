import { Injectable } from '@nestjs/common';
import { OrderIsNotCreatedError } from 'src/common/errors/order.errors';
import { PromiseOrUndefined } from 'src/common/types';
import { OrderDAO } from './order.dao';

@Injectable()
export class OrderService {
  constructor(private orderDao: OrderDAO) {}
  async createOrder(activityOrderInformation): PromiseOrUndefined<number> {
    const creadtedOrderId = await this.orderDao.insertOrder(
      activityOrderInformation,
    );

    //TODO: Somewhat bad place for exception throwing
    if (!creadtedOrderId)
      throw new OrderIsNotCreatedError(
        `Order was not processed because of internal server error`,
      );

    return creadtedOrderId;
  }
}
