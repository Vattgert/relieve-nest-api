import * as format from 'pg-format';

export const INSERT_ORDER_INFO = `
  INSERT INTO orders (activity_id, schedule_id, user_id, host_id) VALUES ($1, $2, $3, $4) RETURNING id;
`;

const INSERT_ORDER_VARIATIONS = `INSERT INTO orders_variations (order_id, variation_id, order_quantity) VALUES %L;`;

export const generateInsertOrderVariationsQuery = (variations: any[][]) => {
  return format(INSERT_ORDER_VARIATIONS, variations);
};
