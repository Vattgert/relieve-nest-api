const GET_ACTIVITY_SCHEDULES = `SELECT schedules.id as "schedule_id", schedules.date::text as "schedule_date",
  timeslots.id as "timeslot_id", timeslots.start_time as "start_time",
  timeslots.end_time as "end_time", 
  schedules_variations.max_spots as "max_spots",
  schedules_variations.available_spots as "available_spots",
  schedules_variations.booked_spots as "booked_spots",
  activities_variations.id as "variation_id",
  activities_variations.quantity as "variation_quantity",
  activities_variations.spots_number as "variation_spots_number",
  activities_variations.title as "variation_title",
  activities_variations.price::numeric as "variation_price",
  activities_variations.duration as "variation_duration",
  currencies.id as "currency_id",
  currencies.name as "currency_name",
  currencies.sign as "currency_sign",
  currencies.code as "currency_code"
  FROM schedules 
  INNER JOIN currencies ON currencies.id = schedules.activity_id
  INNER JOIN timeslots ON schedules.timeslot_id = timeslots.id
  INNER JOIN schedules_variations ON schedules.id = schedules_variations.schedule_id 
  INNER JOIN activities_variations ON schedules_variations.variation_id = activities_variations.id
  WHERE schedules.activity_id = $1;`;

export { GET_ACTIVITY_SCHEDULES };
