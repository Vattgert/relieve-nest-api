import { Inject, Injectable } from '@nestjs/common';
import { DATABASE_SERVICE } from '../storage/database/database.constants';
import { DatabaseService } from '../storage/database/database.service';
import { GET_ACTIVITY_SCHEDULES } from './schedule.queries';
import { Mapper } from '../common/mappers';
import { SCHEDULES_ACTIVITY_SCHEME } from '../common/mappers/schemes';

@Injectable()
export class ScheduleDAO {
  constructor(
    @Inject(DATABASE_SERVICE)
    private databaseService: DatabaseService,
  ) {}

  async getSchedules(activityId: number) {
    const schedules = await this.databaseService.queryAll({
      query: GET_ACTIVITY_SCHEDULES,
      params: [activityId],
    });

    const mapper = new Mapper(schedules, SCHEDULES_ACTIVITY_SCHEME);
    const mappedSchedules = mapper.map();

    return mappedSchedules;
  }
}
