import { Injectable } from '@nestjs/common';
import { ScheduleDAO } from './schedules.dao';

@Injectable()
export default class ScheduleService {
  constructor(private scheduleDao: ScheduleDAO) {}

  async getSchedules(activityId: number) {
    return this.scheduleDao.getSchedules(activityId);
  }
}
