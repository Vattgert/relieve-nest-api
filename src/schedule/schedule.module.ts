import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/storage';
import ScheduleService from './schedule.service';
import { ScheduleDAO } from './schedules.dao';

@Module({
  imports: [DatabaseModule],
  controllers: [],
  providers: [ScheduleService, ScheduleDAO],
  exports: [ScheduleService],
})
export class ScheduleModule {}
