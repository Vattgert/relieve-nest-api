import {
  IsEmail,
  IsNumber,
  ValidateNested,
  IsOptional,
  IsDefined,
  IsCreditCard,
  IsString,
  IsPhoneNumber,
} from 'class-validator';
import { Type } from 'class-transformer';

/*TODO: Price and id are temporarily strings. Change them to numbers later */
class VariationDto {
  @IsString() id: string;
  @IsNumber() quantity: number;
  @IsString() title: string;
  @IsString() price: number;
}

class CreditCardDto {
  cardName: string;
  @IsCreditCard() cardNumber: string;
  cardDate: string;
  cvcCode: number;
}

/*TODO: id is temporarily a string. Change it to number later */
class UserDto {
  @IsOptional() @IsString() id?: string;
  firstName: string;
  lastName: string;
  @IsEmail() email: string;
  @IsPhoneNumber() phoneNumber: string;
}

/*TODO: ActivityId and timeslotId are temporarily strings. Change them to numbers later */
export class BookingActivityDto {
  @IsString() activityId: string;
  @IsString() timeslotId: string;
  @IsString() scheduleId: string;

  @ValidateNested({ each: true })
  @Type(() => VariationDto)
  variations: Array<VariationDto>;

  @IsDefined() creditCard: CreditCardDto;
  @IsDefined() user: UserDto;
}
