import { BookingService } from './booking.service';
import { BookingController } from './booking.controller';
import { Module } from '@nestjs/common';
import { ActivityModule } from 'src/activity/activity.module';
import { OrderModule } from 'src/order/order.module';
import { UserModule } from 'src/user/user.module';
import { SignupModule } from 'src/signup/signup.module';

@Module({
  imports: [ActivityModule, OrderModule, UserModule, SignupModule],
  controllers: [BookingController],
  providers: [BookingService],
  exports: [BookingService],
})
export class BookingModule {}
