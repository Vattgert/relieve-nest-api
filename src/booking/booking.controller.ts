import { Body, Controller, Post, UseFilters } from '@nestjs/common';
import { BusinessErrorFilter } from 'src/common/errors';
import { BookingActivityDto } from './booking.dto';
import { BookingService } from './booking.service';

@Controller('booking')
@UseFilters(BusinessErrorFilter)
export class BookingController {
  constructor(private bookingService: BookingService) {}

  @Post(':activityId')
  async bookActivity(@Body() bookingDto: BookingActivityDto) {
    const orderId = await this.bookingService.bookActivity(bookingDto);
    return { orderId };
  }
}
