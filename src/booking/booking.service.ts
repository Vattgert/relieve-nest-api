import { Injectable } from '@nestjs/common';
import { ActivityService } from 'src/activity/application/activity.service';
import { OrderService } from 'src/order/order.service';
import { SignupService } from 'src/signup/signup.service';
import { UserService } from 'src/user/user.service';
import { BookingActivityDto } from './booking.dto';

@Injectable()
export class BookingService {
  constructor(
    private activityService: ActivityService,
    private orderService: OrderService,
    private userService: UserService,
    private signupService: SignupService,
  ) {}

  async bookActivity(bookingDto: BookingActivityDto): Promise<number> {
    const { activityId, scheduleId, timeslotId, variations, user } = bookingDto;
    const { email } = user;

    const bookedActivity = await this.activityService.getActivityById(
      activityId,
    );
    const { id: hostId } = bookedActivity['host'];
    let purchaserId = user.id
      ? user.id
      : await this.userService.userExists(email);

    if (!purchaserId) {
      const temporaryPassword = this.signupService.generateTemporaryPassword();
      const signupCredentials = { email, password: temporaryPassword };
      const newUserId = await this.signupService.signup(signupCredentials);
      purchaserId = newUserId;
    }

    throw new Error('lol');

    const activityOrderInformation = {
      activityId,
      scheduleId,
      timeslotId,
      hostId,
      userId: purchaserId,
      variations,
    };

    const newOrderId = await this.orderService.createOrder(
      activityOrderInformation,
    );

    return newOrderId;
  }
}
