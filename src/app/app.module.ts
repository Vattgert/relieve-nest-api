import { Module } from '@nestjs/common';

import { ConfigModule } from '@nestjs/config';
import { ActivityModule } from '../activity/activity.module';
import { SignupModule } from '../signup/signup.module';
import { UserModule } from '../user/user.module';
import { CategoryModule } from '../category/category.module';
import { SessionModule } from '../storage';
import { AuthenticationModule } from '../authentication/authentication.module';
import { ScheduleModule } from '../schedule/schedule.module';
import { BookingModule } from '../booking/booking.module';

import { AppController } from './app.controller';
import { AppService } from './app.service';

import configuration from '../config/configuration';
import passwordConfiguration from '../config/encryption';
import { OrderModule } from 'src/order/order.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration, passwordConfiguration],
      envFilePath: ['.env', '.env.password'],
      isGlobal: true,
    }),
    ActivityModule,
    SignupModule,
    UserModule,
    CategoryModule,
    SessionModule,
    AuthenticationModule,
    ScheduleModule,
    OrderModule,
    BookingModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
