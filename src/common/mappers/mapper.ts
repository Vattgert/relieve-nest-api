import { MapMetadata, Map } from './types';

// Inspired by nest-hydration library
export class Mapper {
  private data: any[] | Record<string, any>;
  private scheme: Record<string, any>;

  constructor(data: any[] | Record<string, any>, scheme: Record<string, any>) {
    this.data = data;
    this.scheme = scheme;
  }

  public map() {
    if (this.data.length === 0) {
      return this.data;
    }

    const metadata = this.createMetadata(this.scheme);
    const arrayedData = Array.isArray(this.data) ? this.data : [this.data];

    for (const row of arrayedData) {
      for (const primaryId of metadata.primaryIdColumns) {
        this.mapRecord(row, primaryId, metadata);
      }
    }

    return metadata.result;
  }

  private static isObject(o) {
    return Object.prototype.toString.call(o) === '[object Object]';
  }

  private static isMappedObjectValid(object: Record<string, unknown>) {
    const objectValues = Object.values(object);
    const objectValuesNumber = objectValues.length;
    let badValuesCounter = 0;
    for (const value of objectValues) {
      if (!value) {
        badValuesCounter++;
      }
    }

    return !(objectValuesNumber === badValuesCounter);
  }

  private mapRecord(
    record: Record<string, any>,
    idColumn: string,
    metadata: MapMetadata,
  ) {
    const recordIdColumnValue = record[idColumn];
    const objectMap = metadata.map[idColumn];
    let newObject = {};

    //Default value copy here

    if (objectMap.cache[recordIdColumnValue]) {
      if (objectMap.containingIdUsage === null) {
        // at the top level, parent is root
        return;
      }

      const containingId = record[objectMap.parentIdColumn];

      if (
        objectMap.containingIdUsage[recordIdColumnValue] &&
        objectMap.containingIdUsage[recordIdColumnValue][containingId]
      ) {
        return;
      }

      newObject = objectMap.cache[recordIdColumnValue];
    } else {
      newObject = {};

      objectMap.cache[recordIdColumnValue] = newObject;
      for (const cell of objectMap.valueList) {
        const cellValue = record[cell['column']];

        //Typing here

        newObject[cell['property']] = cellValue;
      }

      //Initialize future arrays
      for (const futureArray of objectMap.mapToMany) {
        newObject[futureArray] = [];
      }

      //Initialize future object
      for (const futureObject of objectMap.mapToOne) {
        newObject[futureObject['property']] = null;
        this.mapRecord(record, futureObject['column'], metadata);
      }
    }

    if (objectMap.parentIdColumn === null) {
      // parent is the top level
      if (objectMap.isOneOfMany) {
        // it is an array
        if (!metadata.result) {
          metadata.result = [];
        }
        //if (Mapper.isMappedObjectValid(newObject))
        (<Array<Record<string, any>>>metadata.result).push(newObject);
      } else {
        // it is this object
        metadata.result = newObject;
      }
    } else {
      const containingId = record[objectMap.parentIdColumn];
      const container =
        metadata.map[objectMap.parentIdColumn].cache[containingId];

      if (container) {
        // it is an array
        if (objectMap.isOneOfMany) {
          //Checking of bad array values, in order to disable
          //in fact empty arrays not be mapped to array with one value like
          // [{ id: null, firstname: null, lastname: null}]
          if (Mapper.isMappedObjectValid(newObject))
            container[objectMap.mapToProperty].push(newObject);
        } else {
          // it is this object
          container[objectMap.mapToProperty] = newObject;
        }
      }

      // record the containing id
      if (!objectMap.containingIdUsage[recordIdColumnValue]) {
        objectMap.containingIdUsage[recordIdColumnValue] = {};
      }

      objectMap.containingIdUsage[recordIdColumnValue][containingId] = true;
    }
  }

  private createMetadata(scheme: Record<string, any>) {
    const metadata: MapMetadata = {
      primaryIdColumns: [],
      map: {},
    };

    if (Array.isArray(scheme)) {
      this.buildMetadata(scheme[0], metadata, true, null, null);
    } else if (Mapper.isObject(scheme)) {
      // register first column as prime id column
      let primeIdColumn = Object.values(scheme)[0];

      if (Mapper.isObject(primeIdColumn)) {
        primeIdColumn = primeIdColumn['column'];
      }

      metadata.primaryIdColumns.push(primeIdColumn);

      // construct the rest
      this.buildMetadata(scheme, metadata, false, null, null);
    }

    return metadata;
  }

  private buildMetadata(
    scheme: Record<string, any>,
    metadata: MapMetadata,
    isOneOfMany,
    parentIdColumn,
    mapToProperty,
  ) {
    const properties = Object.keys(scheme);
    let idProperty: string;

    for (const property of properties) {
      if (scheme[property].id === true) {
        idProperty = property;
        break;
      }
    }

    if (!idProperty) {
      idProperty = properties[0];
    }

    const idColumn = scheme[idProperty].column || scheme[idProperty];

    if (isOneOfMany) {
      metadata.primaryIdColumns.push(idColumn);
    }

    const map: Map = {
      valueList: [],
      mapToOne: [],
      mapToMany: [],
      parentIdColumn,
      mapToProperty,
      isOneOfMany,
      cache: {},
      containingIdUsage: parentIdColumn === null ? null : {},
      default: null,
    };

    for (const property of properties) {
      if (typeof scheme[property] === 'string') {
        map.valueList.push({
          property,
          column: scheme[property],
        });
      } else if (scheme[property]['column']) {
        map.valueList.push({
          property,
          column: scheme[property],
        });
      } else if (Array.isArray(scheme[property])) {
        map.mapToMany.push(property);
        this.buildMetadata(
          scheme[property][0],
          metadata,
          true,
          idColumn,
          property,
        );
      } else if (Mapper.isObject(scheme[property])) {
        let subIdColumn = Object.values(scheme[property])[0];

        if (!subIdColumn) {
          throw new Error(
            `Invalid Scheme format - property ${property} can not be an empty object`,
          );
        }

        if (subIdColumn['column']) {
          subIdColumn = subIdColumn['column'];
        }

        map.mapToOne.push({
          property,
          column: subIdColumn,
        });
        this.buildMetadata(
          scheme[property],
          metadata,
          false,
          idColumn,
          property,
        );
      } else {
        throw new Error(
          `Invalid Scheme format - property ${property} must be either a string, a plain object or an array`,
        );
      }
    }

    metadata.map[idColumn] = map;
  }
}
