export interface MapMetadata {
  primaryIdColumns: string[];
  map?: Record<string, Map>;
  result?: MappingResult;
}

export interface Map {
  valueList: MapValue[];
  mapToOne: any[];
  mapToMany: any[];
  parentIdColumn: null | string;
  mapToProperty: null | string;
  isOneOfMany: boolean;
  cache: Record<string, unknown>;
  containingIdUsage: null | Record<string, any>;
  default: null | string;
}

export interface MapValue {
  property: string;
  column: string;
  type?: any;
  default?: null | string;
}

export type MappingResult = any[] | Record<string, unknown>;
