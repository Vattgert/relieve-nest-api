export const SESSION_SCHEME = {
  id: 'user_id',
  email: 'email',
  firstName: 'first_name',
  lastName: 'last_name',
  username: 'username',
  avatar: 'avatar',
};
