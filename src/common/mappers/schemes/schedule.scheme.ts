const SCHEDULES_ACTIVITY_SCHEME = [
  {
    id: 'schedule_id',
    date: 'schedule_date',
    maxSpots: 'max_spots',
    availableSpots: 'available_spots',
    bookedSpots: 'booked_spots',
    currency: {
      id: 'currency_id',
      name: 'currency_name',
      sign: 'currency_sign',
      code: 'currency_code',
    },
    timeslot: {
      id: 'timeslot_id',
      startTime: 'start_time',
      endTime: 'end_time',
    },
    variations: [
      {
        id: 'variation_id',
        quantity: 'variation_quantity',
        spotsNumber: 'variation_spots_number',
        title: 'variation_title',
        price: 'variation_price',
        duration: 'variation_duration',
      },
    ],
  },
];

export { SCHEDULES_ACTIVITY_SCHEME };
