export const GET_ACTIVITIES_SCHEME = [
  {
    id: 'activity_id',
    title: 'activity_title',
    banner: 'activity_banner',
    country: 'activity_country',
    city: 'activity_city',
    date: 'activity_date',
    summary: 'activity_summary',
    totalLikes: 'total_likes',
    host: {
      id: 'host_id',
      firstName: 'host_first_name',
      lastName: 'host_last_name',
      country: 'host_country',
      city: 'host_city',
      avatar: 'host_avatar',
    },
    category: {
      categoryId: 'category_id',
      categoryTitle: 'category_title',
      categoryCodename: 'category_codename',
      subcategory: {
        subcategoryId: 'subcategory_id',
        subcategoryTitle: 'subcategory_title',
        subcategoryCodename: 'subcategory_codename',
      },
    },
  },
];

export const GET_ACTIVITY_SCHEME = {
  id: 'activity_id',
  title: 'activity_title',
  banner: 'activity_banner',
  country: 'activity_country',
  city: 'activity_city',
  date: 'activity_date',
  summary: 'activity_summary',
  totalLikes: 'total_likes',
  host: {
    id: 'host_id',
    firstName: 'host_first_name',
    lastName: 'host_last_name',
    country: 'host_country',
    city: 'host_city',
    avatar: 'host_avatar',
  },
  category: {
    categoryId: 'category_id',
    categoryTitle: 'category_title',
    categoryCodename: 'category_codename',
    subcategory: {
      subcategoryId: 'subcategory_id',
      subcategoryTitle: 'subcategory_title',
      subcategoryCodename: 'subcategory_codename',
    },
  },
  tags: [
    {
      id: 'tag_id',
      tag: 'tag',
    },
  ],
  lastLikes: [
    {
      id: 'liker_id',
      avatar: 'liker_avatar',
    },
  ],
};

export const ActivityScheme = {
  id: 'activity_id',
  title: 'activity_title',
  banner: 'activity_banner',
  country: 'activity_country',
  city: 'activity_city',
  date: 'activity_date',
  summary: 'activity_summary',
  totalLikes: 'total_likes',
  host: {
    id: 'host_id',
    firstName: 'host_first_name',
    lastName: 'host_last_name',
    avatar: 'host_avatar',
  },
};
