export const USER_PROFILE_SCHEME = {
  id: 'user_id',
  firstName: 'first_name',
  lastName: 'last_name',
  username: 'username',
  summary: 'summary',
  phoneNumber: 'phone_number',
  birthday: 'birthday',
  gender: 'gender',
  avatar: 'avatar',
  city: 'city',
};
