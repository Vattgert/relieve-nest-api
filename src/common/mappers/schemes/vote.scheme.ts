export const VOTE_SCHEME = [
  {
    id: 'vote_id',
    hostGrade: 'host_grade',
    contentGrade: 'content_grade',
    satisfactionGrade: 'satisfaction_grade',
    valueForMoneyGrade: 'value_for_money_grade',
    voter: {
      id: 'voter_id',
      firstName: 'voter_first_name',
      lastName: 'voter_last_name',
      country: 'voter_country',
      city: 'voter_city',
      avatar: 'voter_avatar',
    },
  },
];
