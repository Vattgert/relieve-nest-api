import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
} from '@nestjs/common';
import { ErrorPayload } from './error-payload';
import { UserExistsError } from './user-errors';
import { OrderIsNotCreatedError } from './order.errors';

@Catch(UserExistsError)
@Catch(OrderIsNotCreatedError)
export class BusinessErrorFilter implements ExceptionFilter {
  catch(exception: UserExistsError, host: ArgumentsHost) {
    const response = host.switchToHttp().getResponse();
    const errorPayload: ErrorPayload = {
      message: exception.message,
      httpStatus: HttpStatus.CONFLICT,
    };
    response.status(HttpStatus.CONFLICT).send(errorPayload);
  }
}
