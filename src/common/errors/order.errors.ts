export class OrderIsNotCreatedError extends Error {
  constructor(message) {
    super(message);
  }
}
