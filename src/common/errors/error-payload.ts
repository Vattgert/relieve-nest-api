export interface ErrorPayload {
  message: string;
  explanation?: string;
  action?: string;
  httpStatus?: number;
}
