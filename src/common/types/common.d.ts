export type PromiseOrUndefined<T> = Promise<T | undefined>;
