import { Injectable } from '@nestjs/common';
import { EncryptionOptions } from './encryption-options';
import { genSalt, hash, compare } from 'bcrypt';
import { EncryptionResult } from './interfaces/encryption-result';

@Injectable()
export class EncryptionService {
  private pepper = '';
  private rounds = 1;

  constructor(private encryptionOptions: EncryptionOptions) {
    const { pepper, rounds } = encryptionOptions.getEncryptionOptions();
    this.pepper = pepper;
    this.rounds = rounds;
  }

  async encryptPassword(password): Promise<EncryptionResult> {
    const salt = await genSalt(this.rounds);
    const passwordHash = await hash(password, salt);
    return { passwordHash, salt };
  }

  async checkPassword(password: string, hash: string): Promise<boolean> {
    return await compare(password, hash);
  }
}
