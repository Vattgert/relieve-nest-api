import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { EncryptionOptions } from './encryption-options';
import { EncryptionService } from './encryption.service';

@Module({
  imports: [ConfigService],
  providers: [EncryptionService, EncryptionOptions],
  exports: [EncryptionService],
})
export class EncryptionModule {}
