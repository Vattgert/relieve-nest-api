export interface EncryptionResult {
  passwordHash: string;
  salt: string;
}
