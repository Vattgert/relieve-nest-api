import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class EncryptionOptions {
  constructor(private configService: ConfigService) {}

  getEncryptionOptions() {
    return this.configService.get('encryption');
  }
}
