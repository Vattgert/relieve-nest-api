import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';

import fastifyCookie from 'fastify-cookie';

import { AppModule } from './app/app.module';

async function bootstrap() {
  const API_VERSTION = 'v1';

  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
  );

  app.enableCors({
    credentials: true,
    origin: ['http://localhost:3003', 'http://localhost:8000'],
  });

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
    }),
  );

  app.register(fastifyCookie, {
    secret: 'my-secret-cookie',
  });

  app.setGlobalPrefix(API_VERSTION);
  await app.listen(3000);
}
bootstrap();
