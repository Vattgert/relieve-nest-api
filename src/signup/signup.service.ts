import { Injectable } from '@nestjs/common';
import { EncryptionService } from 'src/encryption/encryption.service';
import { UserExistsError } from 'src/common/errors/user-errors';
import { UserService } from 'src/user/user.service';
import { SignupDto } from './signup.dto';

@Injectable()
export class SignupService {
  constructor(
    private userService: UserService,
    private encryptionService: EncryptionService,
  ) {}

  async signup(signupDto: SignupDto): Promise<any> {
    const { email, password } = signupDto;
    const userExists = await this.userService.userExists(email);

    if (userExists)
      throw new UserExistsError(`User with email ${email} already exists.`);

    const { passwordHash, salt } = await this.encryptionService.encryptPassword(
      password,
    );
    const createdUserId = await this.userService.createUser({
      email,
      passwordHash,
      salt,
    });

    return { userId: createdUserId };
  }

  generateTemporaryPassword(): string {
    const result = [];
    const length = 12;
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result.push(
        characters.charAt(Math.floor(Math.random() * charactersLength)),
      );
    }
    return result.join('');
  }
}
