import { Body, Controller, Post, UseFilters } from '@nestjs/common';
import { BusinessErrorFilter } from '../common/errors';
import { SignupDto } from './signup.dto';
import { SignupService } from './signup.service';

@Controller('signup')
@UseFilters(BusinessErrorFilter)
export class SignupController {
  constructor(private signupService: SignupService) {}

  @Post()
  async signup(@Body() signupDto: SignupDto): Promise<any> {
    return this.signupService.signup(signupDto);
  }
}
