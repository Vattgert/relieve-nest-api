import { DatabaseService } from '../storage/database/database.service';
import { DATABASE_SERVICE } from '../storage/database/database.constants';
import { GET_ACTIVITY_VOTES } from './vote.queries';
import { Inject, Injectable } from '@nestjs/common';
import { Mapper } from '../common/mappers';
import { VOTE_SCHEME } from '../common/mappers/schemes';

@Injectable()
export class VoteDAO {
  constructor(
    @Inject(DATABASE_SERVICE)
    private databaseService: DatabaseService,
  ) {}

  async getVotes(activityId: number) {
    const votes = await this.databaseService.queryAll({
      query: GET_ACTIVITY_VOTES,
      params: [activityId],
    });

    const mapper = new Mapper(votes, VOTE_SCHEME);
    const mappedVotes = mapper.map();
    return mappedVotes;
  }
}
