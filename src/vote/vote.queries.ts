export const GET_ACTIVITY_VOTES = `SELECT votes.id as vote_id, host_grade, satisfaction_grade, content_grade,
  value_for_money_grade, user_profiles.user_id as voter_id, first_name as voter_first_name,
  last_name as voter_last_name, avatar as voter_avatar, city as voter_city, countries.nicename as voter_country  
  FROM votes 
  INNER JOIN user_profiles ON votes.user_id = user_profiles.user_id 
  INNER JOIN countries ON user_profiles.country_id = countries.id
  WHERE votes.activity_id = $1`;
