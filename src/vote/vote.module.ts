import { Module } from '@nestjs/common';
import { VoteService } from './vote.service';
import { DatabaseModule } from '../storage';
import { VoteDAO } from './vote.dao';

@Module({
  imports: [DatabaseModule],
  providers: [VoteService, VoteDAO],
  exports: [VoteService],
})
export class VoteModule {}
