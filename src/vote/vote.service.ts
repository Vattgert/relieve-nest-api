import { Injectable } from '@nestjs/common';
import { VoteDAO } from './vote.dao';

@Injectable()
export class VoteService {
  constructor(private voteDao: VoteDAO) {}

  async getActivityVotes(activityId: number) {
    return this.voteDao.getVotes(activityId);
  }
}
