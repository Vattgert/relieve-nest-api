import { Controller, Get } from '@nestjs/common';
import { CategoryService } from './category.service';

@Controller('categories')
export class CategoryController {
  constructor(private categoryService: CategoryService) {}

  @Get('/top')
  async getTopCategories(): Promise<Record<string, unknown>[]> {
    const topCategories = await this.categoryService.getTopCategories();
    return topCategories;
  }
}
