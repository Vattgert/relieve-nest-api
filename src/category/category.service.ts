import { Injectable } from '@nestjs/common';
import { CategoryDAO } from './category.dao';

@Injectable()
export class CategoryService {
  constructor(private categoryDao: CategoryDAO) {}

  async getTopCategories() {
    const topCategories = await this.categoryDao.getTopCategories();
    return topCategories;
  }
}
