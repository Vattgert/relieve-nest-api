import { Inject, Injectable } from '@nestjs/common';

import { DatabaseService } from 'src/storage/database/database.service';
import { DATABASE_SERVICE } from '../storage/database/database.constants';
import { GET_TOP_CATEGORIES } from './category.queries';

@Injectable()
export class CategoryDAO {
  constructor(
    @Inject(DATABASE_SERVICE)
    private databaseService: DatabaseService,
  ) {}

  async getTopCategories() {
    return this.databaseService.queryAll({ query: GET_TOP_CATEGORIES });
  }
}
