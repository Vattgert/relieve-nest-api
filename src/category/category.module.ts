import { Module } from '@nestjs/common';
import { DatabaseModule } from '../storage';

import { CategoryController } from './category.controller';
import { CategoryDAO } from './category.dao';
import { CategoryService } from './category.service';

@Module({
  imports: [DatabaseModule],
  controllers: [CategoryController],
  providers: [CategoryDAO, CategoryService],
})
export class CategoryModule {}
