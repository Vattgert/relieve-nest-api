export const GET_USER_BY_EMAIL = `SELECT `;
export const CREATE_NEW_USER = `INSERT INTO users (email, password, password_salt) VALUES ($1, $2, $3) RETURNING id as "userId";`;
export const USER_EXISTS = `SELECT id FROM users WHERE email = $1;`;
export const GET_USER_CREDENTIALS = `SELECT id as "userId", password as "passwordHash" FROM users WHERE email = $1`;
export const GET_USER_PROFILE = `SELECT id, email, user_profiles.* FROM users INNER JOIN user_profiles ON users.id = user_profiles.user_id WHERE users.id = $1;`;
