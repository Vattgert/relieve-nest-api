import { Injectable } from '@nestjs/common';
import { Mapper } from 'src/common/mappers';
import { SESSION_SCHEME } from '../common/mappers/schemes';
import { UserDAO } from './user.dao';

@Injectable()
export class UserService {
  constructor(private userDao: UserDAO) {}

  async userExists(email: string): Promise<number> {
    return this.userDao.checkUserExists(email);
  }

  async createUser(userInfo: Record<string, any>) {
    return this.userDao.createNewUser(userInfo);
  }

  async getUserCredentials(email: string): Promise<Record<string, any>> {
    return this.userDao.getUserCredentials(email);
  }

  async getUserProfile(userId: number) {
    return this.userDao.getUserProfile(userId);
  }

  async getUserSessionInfo(userId: number) {
    const userProfile = await this.getUserProfile(userId);
    const mapper = new Mapper(userProfile, SESSION_SCHEME);
    const sessionUserData = mapper.map();
    return sessionUserData;
  }
}
