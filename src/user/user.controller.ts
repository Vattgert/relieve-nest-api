import { Controller, Get, Param } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  async getAllActivities(): Promise<Record<string, unknown>[]> {
    return [{ userId: 1, email: 'vattgert@gmail.com', name: 'Ivan' }];
  }

  @Get('/:id/profiles')
  async getUserProfile(@Param() params): Promise<any> {
    const userId = parseInt(params.id);
    const userProfile = await this.userService.getUserProfile(userId);
    return userProfile;
  }
}
