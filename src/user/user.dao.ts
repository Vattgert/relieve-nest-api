import { Injectable, Inject } from '@nestjs/common';
import { DatabaseService } from 'src/storage/database/database.service';
import { DATABASE_SERVICE } from '../storage/database/database.constants';
import {
  CREATE_NEW_USER,
  USER_EXISTS,
  GET_USER_CREDENTIALS,
  GET_USER_PROFILE,
} from './user.queries';

@Injectable()
export class UserDAO {
  constructor(
    @Inject(DATABASE_SERVICE)
    private databaseService: DatabaseService,
  ) {}

  //This is probably should be a part of UserService, not UserDAO
  async checkUserExists(email: string): Promise<number> {
    const { id } = await this.databaseService.queryOne({
      query: USER_EXISTS,
      params: [email],
    });

    return id;
  }

  async createNewUser(userInfo: Record<string, any>): Promise<number> {
    const { email, passwordHash, salt } = userInfo;

    const { userId } = await this.databaseService.queryOne({
      query: CREATE_NEW_USER,
      params: [email, passwordHash, salt],
    });

    return parseInt(userId) || 0;
  }

  async getUserCredentials(email: string): Promise<Record<string, any>> {
    const userCredentials = await this.databaseService.queryOne({
      query: GET_USER_CREDENTIALS,
      params: [email],
    });

    return userCredentials;
  }

  async getUserProfile(userId: number): Promise<any> {
    const userProfile = await this.databaseService.queryOne({
      query: GET_USER_PROFILE,
      params: [userId],
    });

    return userProfile;
  }
}
