import { Module } from '@nestjs/common';
import { DatabaseModule } from '../storage/';
import { UserController } from './user.controller';
import { UserDAO } from './user.dao';
import { UserService } from './user.service';

@Module({
  imports: [DatabaseModule],
  controllers: [UserController],
  providers: [UserDAO, UserService],
  exports: [UserService],
})
export class UserModule {}
