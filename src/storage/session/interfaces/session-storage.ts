export interface SessionStorage {
  createSessionId(): string;
  saveSessionData(sessionId: string, sessionData: Record<string, unknown>);
  refreshSession(): void;
  sessionExists(sessionId: string): Promise<boolean>;
  destroySession(sessionId: string): Promise<void>;
  destroyAllSessions(): Promise<void>;
}
