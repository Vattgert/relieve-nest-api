import { Injectable } from '@nestjs/common';
import { RedisSessionStorage } from './redis-storage';

@Injectable()
export class SessionService {
  constructor(private redisSessionStorage: RedisSessionStorage) {}

  createSessionId() {
    return this.redisSessionStorage.createSessionId();
  }

  async createSession(
    sessionId: string,
    sessionData: Record<string, unknown>,
  ): Promise<void> {
    const sessionExists = await this.redisSessionStorage.sessionExists(
      sessionId,
    );

    if (!sessionExists) {
      this.redisSessionStorage.saveSessionData(sessionId, sessionData);
    }
  }
}
