import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { RedisSessionStorage } from './redis-storage';
import { SessionService } from './session.service';

@Module({
  imports: [ConfigModule],
  providers: [RedisSessionStorage, SessionService],
  exports: [SessionService],
})
export class SessionModule {}
