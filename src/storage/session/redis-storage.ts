import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SessionStorage } from './interfaces/session-storage';
import { RedisClient } from 'redis';
import { promisify } from 'util';

@Injectable()
export class RedisSessionStorage implements SessionStorage {
  private redisClient: RedisClient;
  private sessionConfig: Record<string, unknown>;
  private redisPromisified: Record<string, (...args) => any>;

  constructor(private configService: ConfigService) {
    const sessioStorageConfig = this.getSessionStorageConfig();
    this.redisClient = new RedisClient(sessioStorageConfig);
    this.sessionConfig = {
      sessionIdLength: 40,
      timeToLive: 120,
    };

    const redisClientReference = this.redisClient;
    this.redisPromisified = {
      set: promisify(redisClientReference.set).bind(redisClientReference),
      setex: promisify(redisClientReference.setex).bind(redisClientReference),
      get: promisify(redisClientReference.get).bind(redisClientReference),
      exists: promisify(redisClientReference.exists).bind(redisClientReference),
      expire: promisify(redisClientReference.expire).bind(redisClientReference),
      destroy: promisify(redisClientReference.del).bind(redisClientReference),
      destroyAll: promisify(redisClientReference.flushall).bind(
        redisClientReference,
      ),
    };
  }

  getSessionStorageConfig() {
    return this.configService.get('session.storage');
  }

  createSessionId(): string {
    const result = [];
    const length = this.sessionConfig['sessionIdLength'];
    const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result.push(
        characters.charAt(Math.floor(Math.random() * charactersLength)),
      );
    }
    return result.join('');
  }

  async saveSessionData(
    sessionId: string,
    sessionData: Record<string, unknown>,
  ): Promise<void> {
    const ttl = this.sessionConfig['timeToLive'];
    const data = JSON.stringify(sessionData);
    const { setex } = this.redisPromisified;

    setex(sessionId, ttl, data);
  }

  refreshSession(): void {
    throw new Error('Method not implemented.');
  }

  async sessionExists(sessionId: string): Promise<boolean> {
    const { exists } = this.redisPromisified;
    return exists(sessionId);
  }

  async destroySession(sessionId: string): Promise<void> {
    const { destroy } = this.redisPromisified;
    destroy(sessionId);
  }

  async destroyAllSessions(): Promise<void> {
    const { destroyAll } = this.redisPromisified;
    destroyAll();
  }
}
