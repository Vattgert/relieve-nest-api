export interface QueryParams {
  query: string;
  params?: Array<any>;
}

export interface Database {
  queryAll(queryParams: QueryParams): Promise<Array<any>>;
  queryOne(queryParams: QueryParams): Promise<any>;
}
