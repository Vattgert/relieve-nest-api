import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DatabaseOptions } from './database.options';
import { PostgresDatabaseProvider } from './database.provider';

@Module({
  imports: [ConfigModule],
  providers: [PostgresDatabaseProvider, DatabaseOptions],
  exports: [PostgresDatabaseProvider],
})
export class DatabaseModule {}
