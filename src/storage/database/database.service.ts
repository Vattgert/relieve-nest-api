import { Database, QueryParams } from './interfaces/database.interface';

import { Pool } from 'pg';

export class DatabaseService implements Database {
  constructor(private readonly pool: Pool) {}

  //Functions can be combined or simplified and typed;

  async queryAll(queryParams: QueryParams): Promise<any[]> {
    try {
      const { query, params } = queryParams;
      const result = await this.pool.query(query, params);
      return result.rows;
    } catch (error) {
      throw new Error('Cannot process query');
    }
  }

  async queryOne(queryParams: QueryParams): Promise<any> {
    try {
      const { query, params } = queryParams;
      const result = await this.pool.query(query, params);
      return result.rows[0];
    } catch (error) {
      throw new Error('Cannot process query');
    }
  }

  async transaction(execution: (param?: any) => any): Promise<any> {
    try {
      await this.pool.query('BEGIN');
      const transactionResult = await execution();
      await this.pool.query('COMMIT');
      return transactionResult;
    } catch (error) {
      console.log(error);
      await this.pool.query('ROLLBACK');
      throw new Error('Database transaction error');
    }
  }
}
