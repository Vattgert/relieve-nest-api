import { Provider } from '@nestjs/common';

import { Pool } from 'pg';

import { DATABASE_SERVICE } from './database.constants';
import { DatabaseOptions } from './database.options';
import { DatabaseService } from './database.service';

export const PostgresDatabaseProvider: Provider = {
  provide: DATABASE_SERVICE,
  useFactory: async (databaseOptions: DatabaseOptions) => {
    const {
      host,
      username,
      port,
      password,
      database,
    } = databaseOptions.getDatabaseOptions();

    const pool = await new Pool({
      host,
      port,
      user: username,
      password,
      database,
      max: 20,
      idleTimeoutMillis: 30000,
      connectionTimeoutMillis: 2000,
    });

    return new DatabaseService(pool);
  },
  inject: [DatabaseOptions],
};
