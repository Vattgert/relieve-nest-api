import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class DatabaseOptions {
  constructor(private configService: ConfigService) {}

  getDatabaseOptions() {
    return this.configService.get('database');
  }
}
