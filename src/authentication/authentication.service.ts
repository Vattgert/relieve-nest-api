import { Injectable } from '@nestjs/common';
import { EncryptionService } from 'src/encryption/encryption.service';
import { SessionService } from 'src/storage/session/session.service';
import { UserService } from 'src/user/user.service';
import { AuthenticationDto } from './authentication.dto';

@Injectable()
export class AuthenticationService {
  constructor(
    private sessionService: SessionService,
    private userService: UserService,
    private encryptionService: EncryptionService,
  ) {}

  async auth(authDto: AuthenticationDto): Promise<Record<string, any> | null> {
    const { email, password } = authDto;
    const userExists = await this.userService.userExists(email);

    if (userExists) {
      const {
        userId,
        passwordHash,
      } = await this.userService.getUserCredentials(email);

      const isTruePassword = await this.encryptionService.checkPassword(
        password,
        passwordHash,
      );

      if (isTruePassword) {
        const userSession = this.userService.getUserSessionInfo(
          parseInt(userId),
        );

        return userSession;
      }
    }
    return null;
  }
}
