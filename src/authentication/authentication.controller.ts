import { Body, Controller, Post } from '@nestjs/common';
import { AuthenticationDto } from './authentication.dto';
import { AuthenticationService } from './authentication.service';

@Controller('auth')
export class AuthenticationController {
  constructor(private authenticationService: AuthenticationService) {}

  @Post()
  async auth(
    @Body() authenticationDto: AuthenticationDto,
  ): Promise<Record<string, any>> {
    const userSessionInfo = await this.authenticationService.auth(
      authenticationDto,
    );

    return userSessionInfo;
  }
}
