export default () => ({
  encryption: {
    pepper: process.env.ENCRYPT_PEPPER,
    rounds: parseInt(process.env.ENCRYPT_SALT_ROUNDS) || 5,
  },
});
