export default () => {
  const env = process.env.NODE_ENV;
  const dev = {
    app: {
      port: parseInt(process.env.APP_PORT) || 3000,
    },
    database: {
      host: process.env.DB_DEV_HOST || 'localhost',
      port: parseInt(process.env.DB_DEV_PORT) || 5432,
      username: process.env.DB_DEV_USER,
      password: process.env.DB_DEV_PASSWORD,
      database: process.env.DB_DEV_NAME,
    },
    session: {
      storage: {
        host: process.env.REDIS_LOCAL_STORAGE_HOST,
        port: process.env.REDIS_LOCAL_STORAGE_PORT,
      },
    },
  };

  const prod = {
    app: {
      port: parseInt(process.env.APP_PORT) || 3000,
    },
    database: {
      host: process.env.DB_PROD_HOST || 'localhost',
      port: parseInt(process.env.DB_PROD_PORT) || 5432,
      username: process.env.DB_PROD_USER,
      password: process.env.DB_PROD_PASSWORD,
      database: process.env.DB_PROD_NAME,
    },
    session: {
      storage: {
        host: process.env.REDIS_LOCAL_STORAGE_HOST,
        port: process.env.REDIS_LOCAL_STORAGE_PORT,
      },
    },
  };

  const config = { dev, prod };

  return config[env];
};
