import { Controller, Get, Param } from '@nestjs/common';
import ScheduleService from 'src/schedule/schedule.service';
import { VoteService } from 'src/vote/vote.service';
import { ActivityService } from '../application/activity.service';

import { GetActivityByIdParamDto, GetActivityVotesByIdParam } from './dto';

@Controller('activities')
export class ActivityController {
  constructor(
    private activityService: ActivityService,
    private voteService: VoteService,
    private scheduleService: ScheduleService,
  ) {}

  @Get()
  async getAllActivities(): Promise<Record<string, unknown>[]> {
    return this.activityService.getAllActivities();
  }

  @Get(':id')
  async getActivity(@Param() param: GetActivityByIdParamDto) {
    try {
      const activity = await this.activityService.getActivityById(param.id);
      return activity;
    } catch (error) {
      return {
        status: 500,
        message: error.message || 'Server error. Please try again later',
      };
    }
  }

  @Get(':id/votes')
  getActivityVotes(@Param() param: GetActivityVotesByIdParam) {
    const activityId = parseInt(param.id);
    return this.voteService.getActivityVotes(activityId);
  }

  @Get(':id/schedules')
  getActivitySchedules(@Param() params) {
    const activityId = parseInt(params.id);

    return this.scheduleService.getSchedules(activityId);
  }
}
