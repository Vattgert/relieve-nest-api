import { IsNotEmpty, IsString } from 'class-validator';

export class GetActivityByIdParamDto {
  @IsString()
  @IsNotEmpty()
  readonly id: string;
}
