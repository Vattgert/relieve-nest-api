import { GetActivityVotesByIdParam } from './get-activity-votes-by-id.param';
import { GetActivityByIdParamDto } from './get-activity-by-id.param';

export { GetActivityByIdParamDto, GetActivityVotesByIdParam };
