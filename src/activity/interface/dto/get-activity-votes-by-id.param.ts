import { IsNotEmpty, IsString } from 'class-validator';

export class GetActivityVotesByIdParam {
  @IsString()
  @IsNotEmpty()
  readonly id: string;
}
