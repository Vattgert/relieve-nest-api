import { Inject, Injectable } from '@nestjs/common';
import { PromiseOrUndefined } from 'src/common/types';
import { Activity } from '../domain/activity';
import { IActivityRepository } from '../domain/activity.repository';

@Injectable()
export class ActivityService {
  constructor(
    @Inject('IActivityRepository')
    private activityRepository: IActivityRepository,
  ) {}

  async getAllActivities(): Promise<any> {
    //TODO: return activities from repository;
    return [];
  }

  async getActivityById(activityId: string): PromiseOrUndefined<Activity> {
    const activity = await this.activityRepository.findById(activityId);
    if (!activity) throw new Error('Not found');
    return activity;
  }
}
