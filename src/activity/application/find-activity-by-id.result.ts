export class FindActivityByIdResult {
  readonly id: string = '';
  readonly title: string = '';
  readonly banner: string = '';
  readonly country: string = '';
  readonly city: string = '';
  readonly date: Date = new Date();
  readonly summary: string = '';
  readonly totalLikes: number = 0;
}
