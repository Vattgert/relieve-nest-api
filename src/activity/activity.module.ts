import { Module } from '@nestjs/common';
import { ActivityController } from './interface/activity.controller';
import { ActivityService } from './application/activity.service';
import { DatabaseModule } from '../storage';
import { ActivityDAO } from './infrastructure/dao/activity.dao';
import { ActivityRepository } from './infrastructure/repositories/activity.repository';
import { VoteModule } from 'src/vote/vote.module';
import { ScheduleModule } from 'src/schedule/schedule.module';

@Module({
  imports: [DatabaseModule, VoteModule, ScheduleModule],
  controllers: [ActivityController],
  providers: [
    ActivityService,
    ActivityDAO,
    {
      provide: 'IActivityRepository',
      useClass: ActivityRepository,
    },
  ],
  exports: [ActivityService],
})
export class ActivityModule {}
