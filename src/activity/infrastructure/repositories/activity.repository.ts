import { Injectable } from '@nestjs/common';
import { Activity, ActivityProperties } from 'src/activity/domain/activity';
import { IActivityRepository } from 'src/activity/domain/activity.repository';
import { PromiseOrUndefined } from 'src/common/types';
import { ActivityDAO } from '../dao/activity.dao';
import { Mapper } from 'src/common/mappers';
import { ActivityScheme } from 'src/common/mappers/schemes';
import { ActivityHost, ActivityHostProperties } from 'src/activity/domain/host';

@Injectable()
export class ActivityRepository implements IActivityRepository {
  constructor(private readonly activityDao: ActivityDAO) {}

  async findById(id: string): PromiseOrUndefined<Activity> {
    const activityResult = await this.activityDao.getActivityById(id);
    const mapper = new Mapper(activityResult, ActivityScheme);
    const mappedActivityResult = mapper.map();
    const activityModel = this.activityMapper(mappedActivityResult);
    return activityModel;
  }

  //TODO: This is a pretty bad approach of mapping db result to domain model
  //Maybe I should use typeorm, but I wouldn't like to do it for now
  //Should implement this in more declarative way
  private activityMapper(source: Record<string, any>): Activity {
    const {
      id,
      title,
      banner,
      country,
      city,
      date,
      summary,
      totalLikes,
      host,
    } = source;

    const hostProperties: ActivityHostProperties = { ...host };
    const activityHost = new ActivityHost(hostProperties);

    const activityProperties: ActivityProperties = {
      id,
      title,
      banner,
      country,
      city,
      date: new Date(date),
      summary,
      totalLikes,
      activityHost,
    };

    return new Activity(activityProperties);
  }
}
