import { Inject, Injectable } from '@nestjs/common';

import { DatabaseService } from 'src/storage/database/database.service';
import { DATABASE_SERVICE } from 'src/storage/database/database.constants';
import {
  GET_ALL_ACTIVITIES,
  GET_ACTIVITY_BY_ID,
  SELECT_ACTIVITY_BY_ID,
} from '../sql/activity.queries';
import {
  GET_ACTIVITIES_SCHEME,
  GET_ACTIVITY_SCHEME,
} from 'src/common/mappers/schemes';
import { Mapper } from 'src/common/mappers';

@Injectable()
export class ActivityDAO {
  constructor(
    @Inject(DATABASE_SERVICE)
    private databaseService: DatabaseService,
  ) {}

  async getAllActivities() {
    const activities = await this.databaseService.queryAll({
      query: GET_ALL_ACTIVITIES,
    });
    const entityMapper = new Mapper(activities, GET_ACTIVITIES_SCHEME);
    const mappedActivities = entityMapper.map();
    return mappedActivities;
  }

  async getActivityById(activityId: string) {
    return this.databaseService.queryOne({
      query: SELECT_ACTIVITY_BY_ID,
      params: [activityId],
    });
  }
}
