export const GET_ALL_ACTIVITIES = `SELECT activities.id as activity_id, activities.title as activity_title, banner as activity_banner,
  activities.country as activity_country, activities.city as activity_city, date as activity_date, user_id as host_id,
  first_name as host_first_name, last_name as host_last_name, countries.nicename as host_country,
  user_profiles.city as host_city, avatar as host_avatar,
  subcategories.id as subcategory_id,
  subcategories.title as subcategory_title,
  subcategories.codename as subcategory_codename,
  categories.id as category_id,
  categories.title as category_title,
  categories.codename as category_codename
  FROM activities 
  INNER JOIN user_profiles ON activities.host_id = user_profiles.user_id 
  INNER JOIN countries ON user_profiles.country_id = countries.id
  INNER JOIN categories as subcategories ON activities.category_id = subcategories.id
  INNER JOIN categories ON subcategories.parent_id = categories.id;`;

export const GET_ACTIVITY_BY_ID = `SELECT activities.id as activity_id, activities.title as activity_title, banner as activity_banner,
  activities.country as activity_country, activities.city as activity_city, date as activity_date,
  activities.summary as activity_summary, 
  subcategories.id as subcategory_id,
  subcategories.title as subcategory_title,
  subcategories.codename as subcategory_codename,
  categories.id as category_id,
  categories.title as category_title,
  categories.codename as category_codename,
  user_id as host_id,
  first_name as host_first_name, last_name as host_last_name, countries.nicename as host_country,
  user_profiles.city as host_city, avatar as host_avatar, tags.id as tag_id, tags.tag, likes.liker_id, likes.liker_avatar,
  (SELECT COUNT(*) FROM likes WHERE likes.activity_id = $1) as total_likes
  FROM activities 
  INNER JOIN user_profiles ON activities.host_id = user_profiles.user_id 
  INNER JOIN countries ON user_profiles.country_id = countries.id 
  INNER JOIN categories as subcategories ON activities.category_id = subcategories.id
  INNER JOIN categories ON subcategories.parent_id = categories.id
  LEFT JOIN activities_tags ON activities_tags.activity_id = activities.id
  LEFT JOIN tags ON activities_tags.tag_id = tags.id
  LEFT JOIN (
    SELECT likes.activity_id, likers.user_id as liker_id, likers.avatar as liker_avatar
    FROM likes INNER JOIN user_profiles as likers ON likes.user_id = likers.user_id 
    WHERE likes.activity_id = $1 LIMIT 5
  ) as likes ON likes.activity_id = activities.id
  WHERE activities.id = $1;`;

export const SELECT_ACTIVITY_BY_ID = `
  SELECT activities.id as activity_id, activities.title as activity_title, banner as activity_banner,
  activities.country as activity_country, activities.city as activity_city, date as activity_date,
  activities.summary as activity_summary,
  user_id as host_id, first_name as host_first_name, last_name as host_last_name, avatar as host_avatar
  FROM activities 
  INNER JOIN user_profiles ON activities.host_id = user_profiles.user_id 
  WHERE activities.id = $1;`;
