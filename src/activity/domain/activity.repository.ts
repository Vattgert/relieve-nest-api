import { Activity } from './Activity';
import { PromiseOrUndefined } from '../../common/types';

export interface IActivityRepository {
  findById(id: string): PromiseOrUndefined<Activity>;
}
