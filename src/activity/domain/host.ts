export interface ActivityHostProperties {
  readonly id: string;
  readonly firstName: string;
  readonly lastName: string;
  readonly avatar: string;
}

export class ActivityHost {
  private readonly id: string;
  private readonly firstName: string;
  private readonly lastName: string;
  private readonly avatar: string;

  constructor(activityHostProperties: ActivityHostProperties) {
    this.id = activityHostProperties.id;
    this.firstName = activityHostProperties.firstName;
    this.lastName = activityHostProperties.lastName;
    this.avatar = activityHostProperties.avatar;
  }
}
