import { ActivityHost } from './host';

export interface ActivityProperties {
  readonly id: string;
  readonly title: string;
  readonly banner: string;
  readonly country: string;
  readonly city: string;
  readonly date: Date;
  readonly summary: string;
  readonly totalLikes: number;
  readonly activityHost: ActivityHost;
}

export class Activity {
  private readonly id: string;
  private readonly title: string;
  private readonly banner: string;
  private readonly country: string;
  private readonly city: string;
  private readonly date: Date;
  private readonly summary: string;
  private readonly totalLikes: number;
  private readonly activityHost: ActivityHost;

  constructor(properties: ActivityProperties) {
    this.id = properties.id;
    this.title = properties.title;
    this.banner = properties.banner;
    this.country = properties.country;
    this.city = properties.city;
    this.date = properties.date;
    this.summary = properties.summary;
    this.totalLikes = properties.totalLikes;
    this.activityHost = properties.activityHost;
  }

  /* TODO for now it's anemic model. Write business logic later */
}
